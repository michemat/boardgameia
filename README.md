Othello game.
With an AI presenting different levels of difficulty.

This AI has been implemented thanks to algorithms from game theory (minimax, alphabet, SSS*).

It will commonly represent all the possible actions in the form of a tree, which will then be analyzed by these algorithms, in order to make the best decisions to be in the best possible dispositions considering the future moves.

Each level of difficulty then corresponds to different depths in the tree (from 1: easy to 6: hard)

**Require java 8**

You can download jdk at https://www.oracle.com/fr/java/technologies/javase/javase-jdk8-downloads.html (Linux x64 Compressed Archive / jdk-8u291-linux-x64.tar.gz)
Decompress and execute with : **jdk-8u291-linux-x64/jdk1.8.0_291/bin/java -jar boardgameia-master.jar**
