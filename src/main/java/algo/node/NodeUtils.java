package algo.node;

import algo.heuristic.HeuristicSimple;
import board.Board;
import board.BoardComputing;
import board.IllegalPositionException;
import javafx.geometry.Pos;
import pawn.Pawn;
import pawn.PawnStd;
import util.Color;
import util.Position;

import java.util.List;

public class NodeUtils {

    private static int count = 0;

    /**
     * Renvoi le type de noeud associé a la couleur black/white.
     */
    public static NodeType getNodeType(Color color) {
        if (color == Color.Black) {
            return NodeType.Max;
        }
        return NodeType.Min;
    }

    /**
     * Renvoi la couleur associté au noeud min/max
     */
    public static Color getColor(NodeType nodeType) {
        if (nodeType == NodeType.Max) {
            return Color.Black;
        }
        return Color.White;
    }

    /**
     * Construit les fils d'un noeud et les ajoutes.
     * @param root qui sera ramifiée de ses fils.
     * @throws IllegalPositionException
     */
    public static void buildChild(Node root, Board board, NodeType nodeType) throws IllegalPositionException {
        Color color = getColor(nodeType);
        BoardComputing computing = board.getBoardComputing();

        for (Position position : computing.getLegalPositions(color)) {
            root.addChild(new Node());
        }
    }

    /**
     * Construit un arbre de profondeur depth
     * @param root racine de l'arbre a construire
     * @param depth profondeur de l'arbre a construire
     * @throws IllegalPositionException
     */
    public static void buildTree(Node root, Board board, NodeType nodeType, int depth) throws IllegalPositionException {
        if (depth < 0) {
            return ;
        }

        Color color = getColor(nodeType);
        BoardComputing computing = board.getBoardComputing();
        List<Position> positionList = computing.getLegalPositions(color);

        if (positionList.size() == 0 || depth == 0) {
            double heuristic = HeuristicSimple.getInstance().getHeuristic(board);
            root.setHeuristic(heuristic);
            return ;
        }

        for (Position position : positionList) {
            Node node = new Node();
            root.addChild(node);
            Board clone = board.clone();
            Pawn pawn = new PawnStd(color);
            clone.setPosition(pawn, position);
            buildTree(node, clone, nodeType.opposite(), depth - 1);
        }
    }
    /*public static void buildTree(Node root, int depth) throws IllegalPositionException {
        if (depth < 1) {
            return ;
        }
        buildChild(root);
        for (Node child : root.getChildren()) {
            buildTree(child, depth - 1);
        }
    }*/

    /**
     * Affiche un l'arborescence
     * @param root
     */
    public static void printTree(Node root) {
        printTreeAux(root, "", true);
    }

    private static void printTreeAux(Node node, String prefix, boolean isTail) {
        List children = node.getChildren();
        System.out.println(prefix + (isTail ? "└── " : "├── ") + "•");
        for (int i = 0; i < children.size() - 1; i++) {
            printTreeAux((Node) children.get(i), prefix + (isTail ? "    " : "│   "), false);
        }
        if (children.size() > 0) {
            printTreeAux((Node) children.get(children.size() - 1), prefix + (isTail ?"    " : "│   "), true);
        }
    }
}
