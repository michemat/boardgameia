package algo.node;

import board.Board;

import java.util.ArrayList;
import java.util.List;

/**
 * Réprésentation d'un Arbre
 * Node est une feuille de l'arbe si getChildren().length > 0, une feuille sinon.
 * Un Node a une heuristique.
 */
public class Node {
    //private Board content;
    //private NodeType nodeType;
    private double heuristic;
    private List<Node> children;

    public Node() {
        //this.nodeType = nodeType;
        children = new ArrayList<>();
    }

    public double getHeuristic() {
        return heuristic;
    }

    public void setHeuristic(double heuristic) {
        this.heuristic = heuristic;
    }

    public List<Node> getChildren() {
        return children;
    }

    public void setChildren(List<Node> children) {
        this.children = children;
    }

    public void addChild(Node node) {
        children.add(node);
    }

    /*public NodeType getNodeType() {
        return nodeType;
    }

    public void setNodeType(NodeType nodeType) {
        this.nodeType = nodeType;
    }*/




    /*public Board getContent() {
        return content;
    }

    public void setContent(Board content) {
        this.content = content;
    }*/
}
