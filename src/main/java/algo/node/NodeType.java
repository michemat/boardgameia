package algo.node;

public enum NodeType {
    Min,
    Max;

    public NodeType opposite() {
        if (this == Min) {
            return Max;
        } else {
            return Min;
        }
    }
}
