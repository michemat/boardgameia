package algo;

import algo.heuristic.HeuristicSimple;
import algo.node.Node;
import algo.node.NodeType;
import algo.node.NodeUtils;
import board.Board;
import board.IllegalPositionException;
import pawn.PawnStd;
import util.Color;
import util.Level;
import util.Position;

import java.util.List;

/**
 * Implémentation de AlphaBeta
 */

public class AlphaBeta implements Algorithm {


    public double AlphaBeta(Node root, int depth, boolean isMaxPlayer, double alpha, double beta) {
        if (root.getChildren().size() == 0) {
            return -root.getHeuristic();
        }
        if (isMaxPlayer) {
            for(Node c : root.getChildren()) {
                alpha = Math.max(alpha, AlphaBeta(c, depth + 1, false, alpha, beta));
                if (alpha >= beta) {
                    System.out.println("Pruning at depth" + depth);
                    break;
                }
            }
            return alpha;
        } else { //root is min
            for(Node c : root.getChildren()) {
                beta = Math.min(beta, AlphaBeta(c, depth + 1, true, alpha, beta));
                if (alpha >= beta) {
                    System.out.println("Pruning at depth" + depth);
                    break;
                }
            }
            return beta;
        }
    }

    @Override
    public int getHeuristic(Node node, NodeType n) {
        return (int) AlphaBeta(node,0,n.equals(NodeType.Max),Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY);
    }

    @Override
    public Position getBestPosition(Board board, Color color, Level level) throws IllegalPositionException {
        Position result = null;
        double max = Double.NEGATIVE_INFINITY;
        double tmp;
        List<Position> positions = board.getBoardComputing().getLegalPositions(color);
        for (Position pos : positions) {
            Node tree = new Node();
            Board clone = board.clone();
            clone.setPosition(new PawnStd(color), pos);
            NodeType nt = NodeUtils.getNodeType(color);
            NodeUtils.buildTree(tree, clone, nt, level.getDepth() - 1);
            if (nt.equals(NodeType.Max)) {
                tmp = this.getHeuristic(tree, NodeType.Min);
            } else {
                tmp = this.getHeuristic(tree, NodeType.Max);
            }

            System.out.println(pos + " -> " + tmp);
            if (tmp >= max) {
                result = pos;
                max = tmp;
            }
        }
        return result;
    }
}
