package algo;

import algo.node.Node;
import algo.node.NodeType;
import board.Board;
import board.IllegalPositionException;
import util.Color;
import util.Level;
import util.Position;

/**
 * Algorithme pour récuperer l'heuristique d'un Noeud.
 */
public interface Algorithm {

    int getHeuristic(Node node, NodeType n);

    Position getBestPosition(Board board, Color color, Level level) throws IllegalPositionException;

}
