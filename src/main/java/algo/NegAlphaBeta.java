package algo;

import algo.node.Node;
import algo.heuristic.HeuristicSimple;
import algo.node.NodeType;
import algo.node.NodeUtils;
import board.Board;
import board.IllegalPositionException;
import pawn.PawnStd;
import util.Color;
import util.Level;
import util.Position;

import java.util.List;

/**
 * Implémentation de NegAlphaBeta
 */
public class NegAlphaBeta implements Algorithm {

    public double getHeuristic(Node root, double alpha, double beta, NodeType nodeType) {
        if (root.getChildren().size() == 0) {
            if (nodeType == NodeType.Min) {
                return -root.getHeuristic();
            } else {
                return root.getHeuristic();
            }
        } else {
            double best = Double.NEGATIVE_INFINITY;
            for(Node child : root.getChildren()) {
                double val = -getHeuristic(child, -beta, -alpha, nodeType.opposite());
                if (val > best) {
                    best = val;
                    if (best > alpha) {
                        alpha = best;
                        if (alpha >= beta) {
                            return best;
                        }
                    }
                }
            }
            return best;
        }
    }

    @Override
    public int getHeuristic(Node node, NodeType n) {
        int result = (int) getHeuristic(node, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY, n);
        return result;
    }

    public Position getBestPosition(Board board, Color color, Level level) throws IllegalPositionException {
        Position result = null;
        double max = Double.NEGATIVE_INFINITY;
        double tmp;
        List<Position> positions = board.getBoardComputing().getLegalPositions(color);
        for (Position pos : positions) {
            Node tree = new Node();
            Board clone = board.clone();
            clone.setPosition(new PawnStd(color), pos);
            NodeUtils.buildTree(tree, clone, NodeUtils.getNodeType(color).opposite(), level.getDepth() - 1);
            tmp = -this.getHeuristic(tree, NodeUtils.getNodeType(color).opposite());
            if (tmp >= max) {
                result = pos;
                max = tmp;
            }
        }
        return result;
    }
}
