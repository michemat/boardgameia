package algo;

import algo.node.Node;
import algo.node.NodeType;
import algo.node.NodeUtils;
import board.Board;
import board.IllegalPositionException;
import pawn.PawnStd;
import util.Color;
import util.Level;
import util.Position;

import java.util.*;
import java.util.concurrent.ConcurrentSkipListSet;

public class SSS implements Algorithm {

    //true = vivant
    //false = resolu
    public double sss (Node root, boolean isMax) {
        Set<Node> explored = new HashSet<>();
        PriorityQueue<SSSelement> l = new PriorityQueue<>(new SSSelementComparator());

        SSSelement premier = new SSSelement(root, isMax, Double.POSITIVE_INFINITY, null, true);
        l.add(premier);
        explored.add(premier.node);

        while(true) {
            //System.out.println(l);

            premier = l.poll();
            if(premier.isAlive == false && premier.node == root) {
                break;
            }

            if (premier.isAlive == true) { //si t vivant
                if (premier.node.getChildren().size() == 0) {
                    l.add(new SSSelement(premier.node, false, Math.min(premier.value, premier.node.getHeuristic()), premier.father, premier.isMax));

                } else if (premier.isMax) {
                    for(Node n : premier.node.getChildren()) {
                        SSSelement ele = new SSSelement(n, true, premier.value, premier, false);
                        l.add(ele);
                        explored.add(ele.node);
                    }

                } else { // min
                    Node filsGauche = premier.node.getChildren().get(0);
                    int i = 0;
                    boolean notFound = false;
                    while (i < premier.node.getChildren().size() && !notFound) {
                        filsGauche = premier.node.getChildren().get(i);
                        if (!explored.contains(filsGauche)) {
                            notFound = true;
                        }
                        ++i;
                    }

                    l.add(new SSSelement(filsGauche, true, premier.value, premier, true));

                }
            } else {  //t resolu
                if (!premier.isMax) { // min

                    l.add(new SSSelement(premier.father.node, false, premier.value, premier.father.father, true));
                    for (SSSelement element : l) {
                        for (Node no : premier.node.getChildren()) {
                            if (element.node.equals(no)) {
                                l.remove(element);
                            }
                        }
                    }
                } else {
                    if (!premier.node.equals(premier.father.node.getChildren().get(premier.father.node.getChildren().size() - 1))) {
                        int indexFrDr = premier.father.node.getChildren().indexOf(premier.node) + 1;
                        l.add(new SSSelement(premier.father.node.getChildren().get(indexFrDr), true, premier.value, premier.father, premier.isMax));
                    } else {
                        l.add(new SSSelement(premier.father.node, false, premier.value, premier.father.father, false));
                    }
                }
            }
        }
        return premier.value;
    }

    @Override
    public int getHeuristic(Node node, NodeType n) {
        return (int) sss(node, n.equals(NodeType.Max));
    }



    public Position getBestPosition(Board board, Color color, Level level) throws IllegalPositionException {
        Position result = null;
        double max = Double.NEGATIVE_INFINITY;
        double tmp;
        List<Position> positions = board.getBoardComputing().getLegalPositions(color);
        for (Position pos : positions) {
            Node tree = new Node();
            Board clone = board.clone();
            clone.setPosition(new PawnStd(color), pos);
            NodeType nt = NodeUtils.getNodeType(color);
            NodeUtils.buildTree(tree, clone, nt, level.getDepth() -1);
            if (nt.equals(NodeType.Min)) {
                tmp = this.getHeuristic(tree, NodeType.Min);
            } else {
                tmp = this.getHeuristic(tree, NodeType.Max);
            }

            System.out.println(pos + " - " + tmp);
            if (tmp >= max) {
                result = pos;
                max = tmp;
            }
        }
        return result;
    }

    private class SSSelement {
        public Node node;
        public boolean isAlive;
        public double value;
        public SSSelement father;
        public boolean isMax;
        SSSelement(Node n, Boolean b, double v, SSSelement f, boolean isMax) {
            node = n;
            isAlive = b;
            value = v;
            father = f;
            this.isMax = isMax;
        }

        @Override
        public String toString() {
            return "(" + node + "," + isAlive + ","  + value + ") " + isMax;
        }
    }

    private class SSSelementComparator implements Comparator<SSSelement> {

        public int compare(SSSelement s1, SSSelement s2) {
            if (s1.value > s2.value) {
                return -1;
            } else if (s1.value == s2.value){
                return 0;
            } else {
                return 1;
            }
        }
    }
}



