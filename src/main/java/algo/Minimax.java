package algo;

import algo.node.Node;
import algo.node.NodeType;
import algo.node.NodeUtils;
import board.Board;
import board.IllegalPositionException;
import pawn.PawnStd;
import util.Color;
import util.Level;
import util.Position;

import java.util.List;

public class Minimax implements  Algorithm{

    public double Minimax(Node root, boolean isMax) {
        double val;
        if (root.getChildren().size() == 0) {
            val = -root.getHeuristic();
        } else {
            if (isMax) {
                val = Double.NEGATIVE_INFINITY;
                for (Node n : root.getChildren()) {
                    val = Math.max(val, Minimax(n, !isMax));
                }
            } else {
                val = Double.POSITIVE_INFINITY;
                for (Node n : root.getChildren()) {
                    val = Math.min(val, Minimax(n, !isMax));
                }
            }
        }
        return val;
    }

    @Override
    public int getHeuristic(Node node, NodeType n) {
        return (int) Minimax(node, n.equals(NodeType.Max));
    }

    @Override
    public Position getBestPosition(Board board, Color color, Level level) throws IllegalPositionException {
        Position result = null;

        double max = Double.NEGATIVE_INFINITY;
        double tmp;

        List<Position> positions = board.getBoardComputing().getLegalPositions(color);

        for (Position pos : positions) {
            Node tree = new Node();
            Board clone = board.clone();
            clone.setPosition(new PawnStd(color), pos);
            NodeType nt = NodeUtils.getNodeType(color);
            NodeUtils.buildTree(tree, clone, nt.opposite(), level.getDepth() - 1);
            if (nt.equals(NodeType.Max)) {
                tmp = this.getHeuristic(tree, NodeType.Min);
            } else {
                tmp = this.getHeuristic(tree, NodeType.Max);
            }

            if (tmp >= max) {
                result = pos;
                max = tmp;
            }
        }
        return result;
    }
}
