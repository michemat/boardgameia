package algo.heuristic;


import algo.node.NodeType;
import board.Board;
import util.Color;
import util.Position;


import java.util.List;

/**
 * Heuristique "simple" d'un plateau de jeu Othelo.
 */
public class HeuristicSimple {

    private static HeuristicSimple instance;
    private int[][] score;

    public static HeuristicSimple getInstance() {
        if (instance == null) {
            instance = new HeuristicSimple();
        }
        return instance;
    }

    private HeuristicSimple() {
        scoreInit();
    }


    public int getHeuristic(Board board) {
        int result = 0;
        List<Position> blackPosition = board.getPositions(Color.Black);
        List<Position> whitePosition = board.getPositions(Color.White);
        for (Position pos : blackPosition) {
            result += score[pos.getX()][pos.getY()];
        }
        for (Position pos : whitePosition) {
            result -= score[pos.getX()][pos.getY()];
        }
        return result;
    }

    private void scoreInit() {
        score = new int[][]{
                {500, -150, 30, 10, 10, 30, -150, 500},
                {-150, -250, 0, 0, 0, 0, -250, -150},
                {30, 0, 1, 2, 2, 1, 0, 30},
                {10, 0, 2, 16, 16, 2, 0, 10},
                {10, 0, 2,  16, 16, 2, 0, 10},
                {30, 0, 1, 2, 2, 1, 0, 30},
                {-150, -250, 0, 0, 0, 0, -250, -150},
                {500, -150, 30, 10, 10, 30, -150, 500},
        };
    }

}
