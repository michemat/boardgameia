package algo;

import algo.node.Node;
import algo.node.NodeType;
import algo.node.NodeUtils;
import board.Board;
import board.IllegalPositionException;
import pawn.PawnStd;
import util.Color;
import util.Level;
import util.Position;

import java.util.List;

public class Negamax implements Algorithm{

    public double Negamax(Node root, boolean isMax) {
        double val;
        if (root.getChildren().size() == 0) {
            val = g(root.getHeuristic(), isMax);
        } else {
            val = Double.NEGATIVE_INFINITY;
            for(Node n : root.getChildren()) {
                val = Math.max(val, -Negamax(n, !isMax));
            }
        }
        return val;
    }

    @Override
    public int getHeuristic(Node node, NodeType n) {
        return (int) Negamax(node, n.equals(NodeType.Max));
    }

    @Override
    public Position getBestPosition(Board board, Color color, Level level) throws IllegalPositionException {
        System.out.println("NEGAMAX");
        Position result = null;
        double max = Double.NEGATIVE_INFINITY;
        double tmp;
        List<Position> positions = board.getBoardComputing().getLegalPositions(color);
        for (Position pos : positions) {
            Node tree = new Node();
            Board clone = board.clone();
            clone.setPosition(new PawnStd(color), pos);
            NodeUtils.buildTree(tree, clone, NodeUtils.getNodeType(color).opposite(), level.getDepth() - 1);
            tmp = -this.getHeuristic(tree, NodeUtils.getNodeType(color).opposite());
            if (tmp >= max) {
                result = pos;
                max = tmp;
            }
        }
        return result;
    }

    private double g(double h, boolean isMax) {
        if(isMax) {
            return h;
        } else {
            return -h;
        }
    }
}
