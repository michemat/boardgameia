package util;

public enum Level {
    Easy("Facile", 1),
    Normal("Normal", 4),
    Hard("Difficile", 6);

    private String label;
    private int depth;

    Level(String label, int depth) {
        this.label = label;
        this.depth = depth;
    }

    public String getLabel() {
        return label;
    }

    public int getDepth() {
        return depth;
    }

    @Override
    public String toString() {
        return label;
    }
}
