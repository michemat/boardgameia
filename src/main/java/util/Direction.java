package util;

/**
 * Enumération de direction
 */
public enum Direction {
    North(0, -1),
    NorthEast(1, -1),
    East(1, 0),
    SouthEast(1, 1),
    South(0, 1),
    SouthWest(-1, 1),
    West(-1, 0),
    NorthWest(-1, -1);

    private int deltaX;
    private int deltaY;

    Direction(int deltaX, int deltaY) {
        this.deltaX = deltaX;
        this.deltaY = deltaY;
    }

    /**
     * Delta X d'une direction
     * @return
     */
    public int getDeltaX() {
        return deltaX;
    }

    /**
     * Delta Y d'une direction
     * @return
     */
    public int getDeltaY() {
        return deltaY;
    }


}
