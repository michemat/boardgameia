package util;

import java.util.Objects;

/**
 * Position 2D (X, Y)
 */
public class Position {

    private int x;
    private int y;

    public Position(int x, int y) {
        this.x = x;
        this.y = y;
    }

    /**
     * Recupere le X de la position (X, Y)
     * @return Position en Abscisse
     */
    public int getX() {
        return x;
    }

    /**
     * Recupere le Y de la position (X, Y)
     * @return Position en Ordonné
     */
    public int getY() {
        return y;
    }

    @Override
    public String toString() {
        return "{" + x + "," + y + "}";
    }

    /**
     * Retourne une nouvelle position depuis la direction
     * @param direction la direction selon laquelle la position va être construire
     * @return la nouvelle position depuis la direction
     */
    public Position fromDirection(Direction direction) {
        return new Position(getX() + direction.getDeltaX(),
                getY() + direction.getDeltaY());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Position position = (Position) o;
        return x == position.x &&
                y == position.y;
    }

    @Override
    public int hashCode() {

        return Objects.hash(x, y);
    }
}
