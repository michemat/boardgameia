package util;

import algo.*;

public enum AlgorithmEnum {
    NegAlphaNeta("NegAlphaBeta", new NegAlphaBeta()),
    SSS("SSS*", new SSS()),
    AlphaBeta("AlphaBeta", new AlphaBeta()),
    Minimax("Minimax", new Minimax()),
    Negamax("Negamax", new Negamax());

    private String label;
    private Algorithm algorithm;

    AlgorithmEnum(String label, Algorithm algorithm) {
        this.label = label;
        this.algorithm = algorithm;
    }

    public String getLabel() {
        return label;
    }

    public Algorithm getAlgorithm() {
        return algorithm;
    }

    @Override
    public String toString() {
        return label;
    }
}
