package util;

/**
 * Couleur d'un pion
 */
public enum Color {
    Black,
    White;

    public Color opposite() {
        if (this == Black) {
            return White;
        }
        return Black;
    }

    @Override
    public String toString() {
        if (this == Black) {
            return "X";
        }
        return "O";
    }
}
