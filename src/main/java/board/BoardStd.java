package board;

import pawn.Pawn;
import util.Color;
import util.Direction;
import util.Position;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static board.BoardConstant.BOARD_LENGTH;

/**
 * Implémentation par défaut de l'interface {@link Board}
 */
public class BoardStd implements Board {
    private BoardComputing computing;
    private Pawn[][] board;

    public BoardStd() {
        board = new Pawn[BOARD_LENGTH][BOARD_LENGTH];
    }

    /**
     * Le computateur de plateau doit être affecté avant toute opération
     * @param boardComputing le computateur de plateau
     */
    public void setBoardComputing(BoardComputing boardComputing) {
        computing = boardComputing;
        computing.setBoardArray(board);
    }

    /**
     * Initialise le plateau de jeu
     * @param init contenant l'initialisation du plateau
     */
    public void initialise(Map<Position, Pawn> init) {
        Pawn pawn;
        for (Position pos : init.keySet()) {
             pawn = init.get(pos);
            board[pos.getX()][pos.getY()] = pawn;
            computing.notify(pos);
        }
    }

    @Override
    public boolean isOver() {
        List<Position> whites = computing.getLegalPositions(Color.White);
        List<Position> blacks = computing.getLegalPositions(Color.Black);
        return whites.size() == 0 && blacks.size() == 0;
    }

    @Override
    public BoardComputing getBoardComputing() {
        return computing;
    }

    @Override
    public void setPosition(Pawn pawn, Position position) throws IllegalPositionException {
        Color color = pawn.getColor();
        List<Direction> directions = computing.getLegalDirections(color, position);
        if (directions.size() == 0 || board[position.getX()][position.getY()] != null) {
            throw new IllegalPositionException();
        }

        board[position.getX()][position.getY()] = pawn;
        computing.notify(position);

        for (Direction direction : directions) {
            computing.flipUntil(position, direction, color);
        }
    }

    @Override
    public List<Position> getPositions(Color color) {
        List<Position> positions = new ArrayList<>();
        for (int i = 0; i < BOARD_LENGTH; i++) {
            for (int j = 0; j < BOARD_LENGTH; j++) {
                Pawn pawn = board[i][j];
                if (pawn != null && pawn.getColor().equals(color)) {
                    positions.add(new Position(i, j));
                }
            }
        }
        return positions;
    }

    @Override
    public BoardStd clone() {
        BoardStd clone = new BoardStd();
        Pawn pawn;
        for (int i = 0; i < BOARD_LENGTH; i++) {
            for (int j = 0; j < BOARD_LENGTH; j++) {
                pawn = board[i][j];
                if (pawn != null) {
                    clone.board[i][j] = pawn.clone();
                }
            }
        }
        clone.setBoardComputing(computing.clone());
        return clone;
    }

    @Override
    public Pawn[][] getBoard() {
        return board;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder("\\ ");
        for (int k = 0; k < BOARD_LENGTH; k++) {
            result.append(k).append(" ");
        }
        result.append("\n");
        for (int i = 0; i < BOARD_LENGTH; i++) {
            result.append(i).append(" ");
            for (int j = 0; j < BOARD_LENGTH; j++) {
                Object o = board[j][i];
                if (o == null) {
                    result.append("- ");
                } else {
                    result.append(board[j][i]).append(" ");
                }
            }
            result.append("\n");
        }
        return result.toString();
    }
}
