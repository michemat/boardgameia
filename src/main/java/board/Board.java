package board;

import pawn.Pawn;
import util.Color;
import util.Position;

import java.util.List;

/**
 * Plateau de jeu othello
 */
public interface Board extends Cloneable {

    /**
     * @return if the game is finished
     */
    boolean isOver();

    /**
     * @return board computing of the board
     */
    BoardComputing getBoardComputing();

    /**
     * Place un pion sur le plateau de jeu
     * @param pawn le pion à placer
     * @param position la position où placer le pion
     * @throws IllegalPositionException lorsque le pion ne peut pas être posé à cette position
     */
    void setPosition(Pawn pawn, Position position) throws IllegalPositionException;


    /**
     * Récupère les positions des pions de la couleur indiquée
     * @param color la couleur des pions recherchés
     * @return les positions des pions de la couleur indiquée
     */
    List<Position> getPositions(Color color);

    /**
     * Clonage
     * @return clone du pion
     */
    Board clone();

    Pawn[][] getBoard();
}
