package board;

import pawn.Pawn;
import util.Color;
import util.Direction;
import util.Position;

import java.util.ArrayList;
import java.util.List;

import static board.BoardConstant.BOARD_LENGTH;

/**
 * Classe d'implémentation standard de BoarComputing
 */
public class BoardComputingStd implements BoardComputing {
    private Pawn[][] board;
    private List<Position> legals;

    public BoardComputingStd() {
        legals = new ArrayList<>();
    }

    @Override
    public void flipUntil(Position position, Direction direction, Color color) {
        Position pos = position.fromDirection(direction);
        Pawn pawn = board[pos.getX()][pos.getY()];

        while (!pawn.getColor().equals(color)) {
            pawn.flip();
            pos = pos.fromDirection(direction);
            pawn = board[pos.getX()][pos.getY()];
        }
    }

    @Override
    public List<Direction> getLegalDirections(Color color, Position position) {
        List<Direction> directions = new ArrayList<>();
        for (Direction direction : Direction.values()) {
            if (isLegalForDirectionColor(position, direction, color)) {
                directions.add(direction);
            }
        }
        return directions;
    }

    @Override
    public List<Position> getLegalPositions(Color color) {
        List<Position> colorLegals = new ArrayList<>();
        for (Position legal : legals) {
            if (isLegalForColor(legal, color)) {
                colorLegals.add(legal);
            }
        }
        return colorLegals;
    }

    @Override
    public void setBoardArray(Pawn[][] value) {
        board = value;
    }

    @Override
    public void notify(Position position) {
        if (legals.contains(position)) legals.remove(position);

        for (Direction direction : Direction.values()) {
            Position pos = position.fromDirection(direction);
            if (checkPosition(pos) && !legals.contains(pos)) {
                if (board[pos.getX()][pos.getY()] == null) legals.add(pos);
            }
        }
    }

    /**
     * Indique si la position est légale pour une couleur étant donnée une unique direction
     * @param position la position
     * @param direction la direction
     * @param color la couleur
     * @return true si la position est légale, false sinon
     */
    private Boolean isLegalForDirectionColor(Position position, Direction direction, Color color) {
        Color opposite = color.opposite();
        Boolean end = false;
        Boolean exist = false;
        Boolean next = false;

        position = position.fromDirection(direction);

        int r = position.getX();
        int c = position.getY();

        while (checkInterval(r) && checkInterval(c) && !end) {
            Pawn pawn = board[r][c];

            if (pawn != null) {
                if (!next) {
                    next = pawn.getColor().equals(opposite);
                    end = !next;
                } else {
                    exist = pawn.getColor().equals(color);
                    end = exist;
                }
            } else {
                end = true;
            }

            r += direction.getDeltaX();
            c += direction.getDeltaY();
        }

        return exist;
    }

    /**
     * Indique si la position est légale à une position pour une couleur
     * @param position la position
     * @param color la couleur
     * @return true si la position est légale, false sinon
     */
    private Boolean isLegalForColor(Position position, Color color) {
        Boolean legal = false;
        for (Direction direction : Direction.values()) {
            legal |= isLegalForDirectionColor(position, direction, color);
            if (legal) break;
        }
        return legal;
    }

    @Override
    public BoardComputingStd clone() {
        BoardComputingStd clone = new BoardComputingStd();
        clone.board = board;
        clone.legals.addAll(legals);
        return clone;
    }

    private Boolean checkPosition(Position position) {
        return checkInterval(position.getX()) && checkInterval(position.getY());
    }

    private Boolean checkInterval(Integer integer) {
        return integer >= 0 && integer < BOARD_LENGTH;
    }
}
