package board;

import pawn.Pawn;
import util.Color;
import util.Direction;
import util.Position;

import java.util.List;

/**
 * Classe de logique des placements
 * La légalité concerne la possibilité de placer une pièce sur une position
 * du plateau.
 */
public interface BoardComputing extends Cloneable {

    /**
     * Retourne les pièces depuis la position vers la direction indiquée
     * selon la couleur donnée.
     *
     * Précondition : La direction fait partie des directions retournés par
     * {@link BoardComputing#getLegalDirections(Color, Position)}
     *
     * @param position position de départ
     * @param direction direction vers laquelle les pions vont être retourné
     * @param color couleur jusqu'à laquelle retourner les jetons
     */
    void flipUntil(Position position, Direction direction, Color color);

    /**
     * @param color la couleur
     * @param position la position
     * @return les directions dans lequels vont être retournés des pions
     * pour la position/couleur donnée.
     */
    List<Direction> getLegalDirections(Color color, Position position);

    /**
     * @param color La couleur du pion que l'on souhaite poser
     * @return les positions où les pions de la couleur
     * indiquée peuvent être posés
     */
    List<Position> getLegalPositions(Color color);

    /**
     * Set the array of the board
     * @param board array of the board
     */
    void setBoardArray(Pawn[][] board);

    /**
     * Notifie un changement sur le board
     * @param position la position du changement
     */
    void notify(Position position);

    /**
     * Cloneable
     */
    BoardComputingStd clone();
}
