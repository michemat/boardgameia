package pawn;

import util.Color;

/**
 * Pion du jeu
 */
public interface Pawn extends Cloneable {

    /**
     * Change de couleur le pion. (i.e retourne le pion)
     */
    void flip();

    /**
     * Indique au pion sur quel board il est placé.
     * @param color
     */
    void setColor(Color color);

    /**
     * Couleur actuelle du pion.
     * @return Couleur du pion.
     */
    Color getColor();

    /**
     * Clonage
     * @return clone du pion
     */
    Pawn clone();
}
