package pawn;

import util.Color;

public class PawnStd implements Pawn {
    private Color color;

    public PawnStd() {
    }

    public PawnStd(Color color) {
        this.color = color;
    }

    @Override
    public void flip() {
        color = color.opposite();
    }

    @Override
    public Color getColor() {
        return color;
    }

    @Override
    public void setColor(Color color) {
        this.color = color;
    }

    @Override
    public Pawn clone() {
        Pawn clone;
        clone = new PawnStd();
        clone.setColor(color);
        return clone;
    }

    @Override
    public String toString() {
        return color.toString();
    }
}
