package player;

import algo.Algorithm;
import algo.node.Node;
import algo.node.NodeType;
import algo.node.NodeUtils;
import board.Board;
import board.IllegalPositionException;
import javafx.geometry.Pos;
import pawn.PawnStd;
import util.Color;
import util.Level;
import util.Position;

import java.util.List;

import static java.lang.Thread.sleep;

public class IAPlayer {

    private Algorithm algorithm;
    private Level level;
    private Color color;

    public IAPlayer(Algorithm algorithm, Level level, Color color) {
        this.algorithm = algorithm;
        this.level = level;
        this.color = color;
    }

    public void setLevel(Level level) {
        this.level = level;
    }

    public void setAlgorithm(Algorithm algorithm) {
        this.algorithm = algorithm;
    }

    public Position play(Board board) throws IllegalPositionException {
        return algorithm.getBestPosition(board, color, level);
    }

    /*public Position play(Board board) throws IllegalPositionException {
        Position result = null;
        double max = Double.NEGATIVE_INFINITY;
        double tmp;
        List<Position> positions = board.getBoardComputing().getLegalPositions(color);
        for (Position pos : positions) {
            Node tree = new Node();
            Board clone = board.clone();
            clone.setPosition(new PawnStd(color), pos);
            NodeUtils.buildTree(tree, clone, NodeUtils.getNodeType(color), level.getDepth());
            tmp = -algorithm.getHeuristic(tree);
            System.out.println(pos + " -> " + tmp);
            if (tmp >= max) {
                result = pos;
                max = tmp;
            }
        }
        return result;
    }*/
}
