package ui;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import javax.print.DocFlavor;
import java.io.File;
import java.net.URL;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("ui.fxml"));

        primaryStage.setTitle("Reversi");
        primaryStage.setScene(new Scene(root, 480, 630));
        primaryStage.setResizable(false);
        primaryStage.getScene().getStylesheets().add("cell.css");
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
