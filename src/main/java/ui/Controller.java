package ui;

import algo.Algorithm;
import board.IllegalPositionException;
import game.Game;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.RadioButton;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.stage.Popup;
import pawn.Pawn;
import util.AlgorithmEnum;
import util.Color;
import util.Level;
import util.Position;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

public class Controller implements Observer {

    private Pane[][] cells;
    private List<Position> validPosition;

    private Game gameModel;
    private Algorithm algorithm;
    private Level level;


    @FXML GridPane gridBoard;
    @FXML ComboBox chooseAlgorithm;
    @FXML RadioButton easy;
    @FXML RadioButton normal;
    @FXML RadioButton hard;

    private void createModel(Algorithm algo, Level level) {
        gameModel = new Game(algo, level);
    }

    @FXML
    public void initialize() {

        gridBoard.setGridLinesVisible(true);
        cells = new Pane[8][8];
        validPosition = new ArrayList<>();

        easy.setText(Level.Easy.getLabel() + " (" + Level.Easy.getDepth() + ")");
        normal.setText(Level.Normal.getLabel() + " (" + Level.Normal.getDepth() + ")");
        hard.setText(Level.Hard.getLabel() + " (" + Level.Hard.getDepth() + ")");

        if (algorithm == null) {
            chooseAlgorithm.getItems().addAll(AlgorithmEnum.values());
            chooseAlgorithm.getSelectionModel().selectFirst();
            AlgorithmEnum algorithmEnum = (AlgorithmEnum) chooseAlgorithm.getValue();
            algorithm = algorithmEnum.getAlgorithm();
        }

        if (level == null) {
            createModel(algorithm, Level.Normal);
        } else {
            createModel(algorithm, level);
        }
        createController();


        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                final Pane p = new Pane();
                cells[i][j] = p;

                final int x = i;
                final int y = j;
                p.setOnMouseClicked(new EventHandler<MouseEvent>() {
                    public void handle(MouseEvent me) {
                        Position playPosition = new Position(x, y);
                        System.out.println("Je veux jouer ici : " + playPosition);
                        try {
                            gameModel.play(playPosition);
                        } catch (IllegalPositionException e) {
                            System.out.println("Erreur : Invalide Position");
                        }
                    }
                });

                p.getStyleClass().clear();
                p.getStyleClass().add("cell-style");
                gridBoard.add(p, i, j);
            }
        }

        updateValidPosition(Color.Black);
        printValidePosition();
        updateGrid(gameModel.getBoard());

    }

    private void createController() {
        gameModel.addObserver(new Observer() {
            @Override
            public void update(Observable o, Object arg) {
                if (arg.equals("human")) {
                    updateGrid(gameModel.getBoard());
                    resetValidPosition();
                }
                if (arg.equals("ia")) {
                    updateGrid(gameModel.getBoard());
                    updateValidPosition(Color.Black);
                    printValidePosition();
                }
                if (arg.equals("finish")) {
                    updateGrid(gameModel.getBoard());
                    resetValidPosition();
                    int nbPawnBlack = gameModel.getNbPawn(Color.Black);
                    int nbPawnWhite = gameModel.getNbPawn(Color.White);
                    String title, header, content;
                    if (nbPawnBlack > nbPawnWhite) {
                        title = "Gagné ! :)";
                        header = "Wow !";
                        content = "Vous avez gagné !\n Score : "
                                + nbPawnBlack + " pions noirs et " + nbPawnWhite + " pions blancs";
                    } else if(nbPawnBlack == nbPawnWhite) {
                        title = "Egalité ! :/";
                        header = "Egalité !";
                        content = "Vous êtes aussi fort que l'IA !\n Score : "
                                + nbPawnBlack + " pions noirs et " + nbPawnWhite + " pions blancs";
                    } else {
                        title = "Perdu ! :(";
                        header = "Mince ...";
                        content = "Vous ferez sans doute mieux la prochaine fois ...\n Score : "
                                + nbPawnBlack + " pions noirs et " + nbPawnWhite + " pions blancs";
                    }
                    showPopup(title, header, content);
                }
            }
        });
    }

    private void showPopup(String title, String header, String content) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle(title);
        alert.setHeaderText(header);
        alert.setContentText(content);
        alert.showAndWait();
    }

    private void updateGrid(final Pawn[][] board) {
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                if (board[i][j] != null) {
                    final Pane p = cells[i][j];
                    p.getChildren().clear();
                    Pawn pawn = board[i][j];

                    if (pawn.getColor() == Color.Black) {
                        ImageView iv1 = new ImageView(new Image(getClass().getClassLoader().getResource("noir.png").toExternalForm()));
                        iv1.relocate(8, 10);
                        p.getChildren().add(iv1);
                    } else {
                        ImageView iv1 = new ImageView(new Image(getClass().getClassLoader().getResource("blanc.png").toExternalForm()));
                        iv1.relocate(8, 10);
                        p.getChildren().add(iv1);
                    }
                }
            }
        }
    }

    /**
     * Enleve toute les valides positions.
     */
    public void resetValidPosition() {
        for (Position p : validPosition) {
            cells[p.getX()][p.getY()].getStyleClass().clear();
            cells[p.getX()][p.getY()].getStyleClass().add("cell-style");
        }
        validPosition.clear();
    }

    /**
     *
     */
    public void updateValidPosition(Color color) {
        validPosition = gameModel.getValidePosition(color);
    }

    public void printValidePosition() {
        for (Position position : validPosition) {
            System.out.print(position + " ");
            final Pane p = cells[position.getX()][position.getY()];
            p.getStyleClass().clear();
            p.getStyleClass().add("valid-cell-style");
        }
    }

    public boolean isValidPosition(Position p) {
        return validPosition.contains(p);
    }

    /**
     * Changement de la difficulté.
     */
    public void setEasy() {
        System.out.println("Level selected : easy");
        gameModel.setLevel(Level.Easy);
        level = Level.Easy;
    }

    public void setNormal() {
        System.out.println("Level selected : normal");
        gameModel.setLevel(Level.Normal);
        level = Level.Normal;
    }

    public void setHard() {
        System.out.println("Level selected : hard");
        gameModel.setLevel(Level.Hard);
        level = Level.Hard;
    }

    public void setAlgorithm() {
        AlgorithmEnum algorithmEnum = (AlgorithmEnum) chooseAlgorithm.getValue();
        System.out.println("Algo selected : " + algorithmEnum.getLabel());
        gameModel.setAlgorithm(algorithmEnum.getAlgorithm());
    }

    public void resetGame() {
        initialize();
    }

    @Override
    public void update(Observable o, Object arg) {

    }
}
