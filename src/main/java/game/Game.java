package game;

import algo.Algorithm;
import board.Board;
import board.BoardComputingStd;
import board.BoardStd;
import board.IllegalPositionException;
import pawn.Pawn;
import pawn.PawnStd;
import player.IAPlayer;
import util.Color;
import util.Level;
import util.Position;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Observable;

import static java.lang.Thread.sleep;

public class Game extends Observable {

    private Board board;
    private IAPlayer ia;
    private boolean playerTurn;
    private boolean finish;

    public Game(Algorithm algo, Level level) {
        Map<Position, Pawn> map = new HashMap<>();
        map.put(new Position(3, 3), new PawnStd(Color.White));
        map.put(new Position(3, 4), new PawnStd(Color.Black));
        map.put(new Position(4, 3), new PawnStd(Color.Black));
        map.put(new Position(4, 4), new PawnStd(Color.White));
        BoardStd board = new BoardStd();
        board.setBoardComputing(new BoardComputingStd());
        board.initialise(map);
        this.board = board;
        ia = new IAPlayer(algo, level, Color.White);
        playerTurn = true;
    }

    public void play(Position position) throws IllegalPositionException {
        if (finish) {
            return ;
        }
        if (isPlayerTurn()) {
            board.setPosition(new PawnStd(Color.Black), position);
            playerTurn = false;
            updateFinish();
        }
        setChanged();
        notifyObservers("human");
        if (canPlay(Color.White)) {
            playIA();
        } else {
            playerTurn = true;
            System.out.println("JE PASSE MON TOUR");
            setChanged();
            notifyObservers("ia");
        }
    }

    public boolean isPlayerTurn() {
        return playerTurn;
    }

    private void playIA() throws IllegalPositionException {
        if (finish) {
            return ;
        }
        Position pos = ia.play(board);
        board.setPosition(new PawnStd(Color.White), pos);
        updateFinish();
        if (canPlay(Color.Black)) {
            playerTurn = true;
            setChanged();
            notifyObservers("ia");
        } else {
            playIA();
        }
    }

    public List<Position> getValidePosition(Color color) {
        return board.getBoardComputing().getLegalPositions(color);
    }

    private void updateFinish() {
        finish = board.isOver();
        if (finish) {
            setChanged();
            notifyObservers("finish");
        }
    }

    private boolean canPlay(Color color) {
        return board.getBoardComputing().getLegalPositions(color).size() != 0;
    }

    public void setLevel(Level level) {
        ia.setLevel(level);
    }

    public void setAlgorithm(Algorithm algo) {
        ia.setAlgorithm(algo);
    }

    public Pawn[][] getBoard() {
        return board.getBoard();
    }

    public int getNbPawn(Color c) {
        Pawn[][] p = getBoard();
        int nbPawn = 0;
        for (Pawn[] p1 : p) {
            for (Pawn pa : p1) {
                if (pa.getColor().equals(c)) {
                    ++nbPawn;
                }
            }
        }
        return nbPawn;
    }

    @Override
    public String toString() {
        String result = "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n";
        result += "\t Tour de ";
        if (isPlayerTurn()) {
            result += "Joueur";
        } else {
            result += "IA";
        }
        result += "\n\n";
        result +=  board.toString();
        return result;
    }
}
