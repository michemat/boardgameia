package board;

import org.junit.Test;
import pawn.Pawn;
import pawn.PawnStd;
import util.Color;
import util.Position;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BoardComputingStdTest {

    @Test
    public void getLegalPositions() {
        // Given
        Map<Position, Pawn> map = new HashMap<>();
        map.put(new Position(3, 3), new PawnStd(Color.Black));
        map.put(new Position(3, 4), new PawnStd(Color.White));
        map.put(new Position(4, 3), new PawnStd(Color.White));
        map.put(new Position(4, 4), new PawnStd(Color.Black));

        BoardStd board = new BoardStd();
        board.initialise(map);
        // When
        List<Position> positions = board.getBoardComputing().getLegalPositions(Color.Black);

        // Then
        System.out.println(positions);
    }

    @Test
    /**
     * https://prnt.sc/jgsjmd
     */
    public void getLegalPositionsComplex() {
        // Given
        Map<Position, Pawn> map = new HashMap<>();
        map.put(new Position(2, 2), new PawnStd(Color.White));
        map.put(new Position(3, 2), new PawnStd(Color.Black));
        map.put(new Position(5, 2), new PawnStd(Color.White));

        map.put(new Position(2, 3), new PawnStd(Color.Black));
        map.put(new Position(3, 3), new PawnStd(Color.White));
        map.put(new Position(4, 3), new PawnStd(Color.White));

        map.put(new Position(2, 4), new PawnStd(Color.Black));
        map.put(new Position(3, 4), new PawnStd(Color.White));
        map.put(new Position(4, 4), new PawnStd(Color.White));
        map.put(new Position(5, 4), new PawnStd(Color.White));

        map.put(new Position(2, 5), new PawnStd(Color.Black));
        map.put(new Position(4, 5), new PawnStd(Color.Black));

        BoardStd board = new BoardStd();
        board.setBoardComputing(new BoardComputingStd());
        board.initialise(map);
        // When
        List<Position> positions = board.getBoardComputing().getLegalPositions(Color.Black);

        // Then
        System.out.println("Position attenfdu : ");
        System.out.println("[{4,2}, {2,1}, {5,3}, {6,3}, {6,4}, {6,5}, {1,2}, {3,5}, {6,1}]");
        System.out.println("Position obtenues : ");
        System.out.println(positions);
    }
}