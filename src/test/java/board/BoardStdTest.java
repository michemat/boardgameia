package board;

import org.junit.Test;
import pawn.Pawn;
import pawn.PawnStd;
import util.Color;
import util.Position;

import java.util.HashMap;
import java.util.Map;


public class BoardStdTest {
    @Test
    public void getBoardComputing() throws IllegalPositionException {
        // Given
        Map<Position, Pawn> map = new HashMap<>();
        map.put(new Position(3, 3), new PawnStd(Color.White));
        map.put(new Position(3, 4), new PawnStd(Color.Black));
        map.put(new Position(4, 3), new PawnStd(Color.Black));
        map.put(new Position(4, 4), new PawnStd(Color.White));

        BoardStd board = new BoardStd();
        board.setBoardComputing(new BoardComputingStd());
        board.initialise(map);
        System.out.println(board);

        BoardComputing computing = board.getBoardComputing();
        board.setPosition(new PawnStd(Color.White), computing.getLegalPositions(Color.White).get(0));
        System.out.println(board);

        board.setPosition(new PawnStd(Color.Black), computing.getLegalPositions(Color.Black).get(0));
        System.out.println(board);

    }

    @Test
    public void getBoardClone() throws IllegalPositionException {
        // Given
        Map<Position, Pawn> map = new HashMap<>();
        map.put(new Position(3, 3), new PawnStd(Color.White));
        map.put(new Position(3, 4), new PawnStd(Color.Black));
        map.put(new Position(4, 3), new PawnStd(Color.Black));
        map.put(new Position(4, 4), new PawnStd(Color.White));

        BoardStd board = new BoardStd();
        board.setBoardComputing(new BoardComputingStd());
        board.initialise(map);

        // When
        Board clone = board.clone();

        // Then
        System.out.println(board);
        System.out.println(clone);

        System.out.println(board.getBoardComputing().getLegalPositions(Color.White));
        System.out.println(clone.getBoardComputing().getLegalPositions(Color.White));

    }

    @Test
    public void setPosition() throws IllegalPositionException {
        // Given
        Map<Position, Pawn> map = new HashMap<>();
        map.put(new Position(3, 3), new PawnStd(Color.White));
        map.put(new Position(3, 4), new PawnStd(Color.Black));
        map.put(new Position(4, 3), new PawnStd(Color.Black));
        map.put(new Position(4, 4), new PawnStd(Color.White));

        BoardStd board = new BoardStd();
        board.setBoardComputing(new BoardComputingStd());
        board.initialise(map);

        Pawn pawn = new PawnStd(Color.Black);

        // When
        board.setPosition(pawn, new Position(3,2));

        // Then
        System.out.println(board);

    }
}