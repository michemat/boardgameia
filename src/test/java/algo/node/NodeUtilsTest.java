package algo.node;

import algo.NegAlphaBeta;
import board.BoardComputingStd;
import board.BoardStd;
import board.IllegalPositionException;
import org.junit.Test;
import pawn.Pawn;
import pawn.PawnStd;
import util.Color;
import util.Position;

import java.util.HashMap;
import java.util.Map;

public class NodeUtilsTest {

    @Test
    public void buildChild() throws IllegalPositionException {
        // Given
        Map<Position, Pawn> map = new HashMap<>();
        map.put(new Position(3, 3), new PawnStd(Color.Black));
        map.put(new Position(3, 4), new PawnStd(Color.White));
        map.put(new Position(4, 3), new PawnStd(Color.White));
        map.put(new Position(4, 4), new PawnStd(Color.Black));

        BoardStd board = new BoardStd();
        board.setBoardComputing(new BoardComputingStd());
        board.initialise(map);

        Node boardNode = new Node();
        //boardNode.setContent(board);

        // When
        NodeUtils.buildChild(boardNode, board, NodeType.Max);

        // Then
        NodeUtils.printTree(boardNode);
    }

    @Test
    public void buildTree() throws IllegalPositionException {
        // Given
        Map<Position, Pawn> map = new HashMap<>();
        map.put(new Position(3, 3), new PawnStd(Color.Black));
        map.put(new Position(3, 4), new PawnStd(Color.White));
        map.put(new Position(4, 3), new PawnStd(Color.White));
        map.put(new Position(4, 4), new PawnStd(Color.Black));

        BoardStd board = new BoardStd();
        board.setBoardComputing(new BoardComputingStd());
        board.initialise(map);

        Node boardNode = new Node();

        // When
        NodeUtils.buildTree(boardNode, board, NodeType.Max,9);

        // Then
        //NodeUtils.printTree(boardNode);
        //System.out.println(boardNode.getChildren().get(0).getHeuristic());
    }
}