package algo;

import algo.node.Node;
import algo.node.NodeType;
import algo.node.NodeUtils;
import board.Board;
import board.BoardComputingStd;
import board.BoardStd;
import board.IllegalPositionException;
import org.junit.Test;
import pawn.Pawn;
import pawn.PawnStd;
import util.Color;
import util.Level;
import util.Position;

import java.util.HashMap;
import java.util.Map;

public class SSSTest {


    @Test
    public void getHeuristic() throws IllegalPositionException {
        // GIVEN
        Node node = new Node();
        Map<Position, Pawn> map = new HashMap<>();
        map.put(new Position(3, 3), new PawnStd(Color.Black));
        map.put(new Position(3, 4), new PawnStd(Color.White));
        map.put(new Position(4, 3), new PawnStd(Color.White));
        map.put(new Position(4, 4), new PawnStd(Color.Black));
        //map.put(new Position(3, 2), new PawnStd(Color.Black));
        BoardStd board = new BoardStd();
        board.setBoardComputing(new BoardComputingStd());
        board.initialise(map);

        NodeUtils.buildTree(node, board, NodeType.Max, 3);
        SSS s = new SSS();

        // WHEN
        double d = s.getHeuristic(node, NodeType.Max);

        // THEN
        System.out.println("Heuristique : " + d);

    }
    @Test
    public void getBestPosition() throws IllegalPositionException {
        // GIVEN
        Node node = new Node();
        Map<Position, Pawn> map = new HashMap<>();
        map.put(new Position(3, 3), new PawnStd(Color.Black));
        map.put(new Position(3, 4), new PawnStd(Color.White));
        map.put(new Position(4, 3), new PawnStd(Color.White));
        map.put(new Position(4, 4), new PawnStd(Color.Black));
        //map.put(new Position(3, 2), new PawnStd(Color.Black));
        BoardStd board = new BoardStd();
        board.setBoardComputing(new BoardComputingStd());
        board.initialise(map);

        NodeUtils.buildTree(node, board, NodeType.Max, 3);
        SSS s = new SSS();

        // WHEN
        Position position = s.getBestPosition(board, Color.White, Level.Easy);

        // THEN
        System.out.println("Position : " + position);


    }
}

