package algo.heuristic;

import algo.Algorithm;
import algo.NegAlphaBeta;
import algo.node.Node;
import algo.node.NodeType;
import algo.node.NodeUtils;
import board.BoardComputingStd;
import board.BoardStd;
import board.IllegalPositionException;
import org.junit.Test;
import pawn.Pawn;
import pawn.PawnStd;
import player.IAPlayer;
import util.Color;
import util.Level;
import util.Position;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;

public class HeuristicSimpleTest {

    @Test
    public void getHeuristic() throws IllegalPositionException {
        // GIVEN
        Map<Position, Pawn> map = new HashMap<>();
        map.put(new Position(1, 2), new PawnStd(Color.Black));
        map.put(new Position(2, 2), new PawnStd(Color.White));

        map.put(new Position(4, 5), new PawnStd(Color.White));
        map.put(new Position(4, 6), new PawnStd(Color.Black));

        map.put(new Position(3, 7), new PawnStd(Color.Black));

        map.put(new Position(5, 7), new PawnStd(Color.Black));

        map.put(new Position(6,7), new PawnStd(Color.Black));

        BoardStd board = new BoardStd();
        board.setBoardComputing(new BoardComputingStd());
        board.initialise(map);

        Node root = new Node();
        //WHEN

        NodeUtils.buildTree(root, board, NodeType.Min, 1);

        Algorithm algorithm = new NegAlphaBeta();

        IAPlayer player = new IAPlayer(algorithm, Level.Hard, Color.White);

        System.out.println(player.play(board));
        //THEN
    }
}