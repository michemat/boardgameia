package algo;

import algo.node.NodeType;
import algo.heuristic.HeuristicSimple;
import board.BoardStd;
import org.junit.Test;
import pawn.Pawn;
import pawn.PawnStd;
import util.Color;
import util.Position;

import java.util.HashMap;
import java.util.Map;

public class ReversiHeuristicTest {

    @Test
    public void getHeuristicInitialize() {
        // GIVEN
        Map<Position, Pawn> map = new HashMap<>();
        map.put(new Position(3, 3), new PawnStd(Color.Black));
        map.put(new Position(3, 4), new PawnStd(Color.White));
        map.put(new Position(4, 3), new PawnStd(Color.White));
        map.put(new Position(4, 4), new PawnStd(Color.Black));
        BoardStd board = new BoardStd();
        board.initialise(map);

        // WHEN
        int h = HeuristicSimple.getInstance().getHeuristic(board);

        // THEN
        System.out.println("Heuristique : " + h);
    }

    @Test
    public void getHeuristic() {
        // GIVEN
        Map<Position, Pawn> map = new HashMap<>();
        map.put(new Position(3, 3), new PawnStd(Color.Black));
        map.put(new Position(3, 4), new PawnStd(Color.Black));
        map.put(new Position(4, 3), new PawnStd(Color.Black));
        map.put(new Position(4, 4), new PawnStd(Color.White));
        map.put(new Position(3, 2), new PawnStd(Color.Black));
        BoardStd board = new BoardStd();
        board.initialise(map);

        // WHEN
        int h = HeuristicSimple.getInstance().getHeuristic(board);

        // THEN
        System.out.println(board);
        System.out.println("Heuristique : " + h);
    }
}