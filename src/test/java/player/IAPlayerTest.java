package player;

import algo.Algorithm;
import algo.NegAlphaBeta;
import algo.node.Node;
import board.BoardComputingStd;
import board.BoardStd;
import board.IllegalPositionException;
import org.junit.Test;
import pawn.Pawn;
import pawn.PawnStd;
import util.Color;
import util.Level;
import util.Position;

import java.util.HashMap;
import java.util.Map;

public class IAPlayerTest {

    @Test
    public void play() throws IllegalPositionException {
        // GIVEN
        Map<Position, Pawn> map = new HashMap<>();
        map.put(new Position(1, 2), new PawnStd(Color.Black));
        map.put(new Position(2, 2), new PawnStd(Color.White));

        map.put(new Position(4, 5), new PawnStd(Color.White));
        map.put(new Position(4, 6), new PawnStd(Color.Black));

        map.put(new Position(3, 7), new PawnStd(Color.Black));

        map.put(new Position(5, 7), new PawnStd(Color.Black));

        map.put(new Position(6,7), new PawnStd(Color.Black));

        BoardStd board = new BoardStd();
        board.setBoardComputing(new BoardComputingStd());
        board.initialise(map);


        IAPlayer ia = new IAPlayer(new NegAlphaBeta(), Level.Normal, Color.White);

        //WHEN
        Position position = ia.play(board);

        //THEN
        System.out.println("Result : " + position);
    }
/*
    @Test
    public void playCustomTree() throws IllegalPositionException {
        // GIVEN
        Node A = new Node();
        Node B = new Node();
        Node C = new Node();
        Node D = new Node();
        Node E = new Node();
        Node F = new Node();
        Node G = new Node();

        D.setHeuristic(4);
        E.setHeuristic(5);
        F.setHeuristic(-1);
        G.setHeuristic(2);

        A.addChild(B);
        A.addChild(C);

        B.addChild(D);
        B.addChild(E);

        C.addChild(F);
        C.addChild(G);

        double tmp;
        double max = Double.NEGATIVE_INFINITY;
        Algorithm algo = new NegAlphaBeta();

        for (Node child : A.getChildren()) {
            tmp = -algo.getHeuristic(child);
            if (tmp >= max) {
                max = tmp;
            }
        }


        //THEN
        System.out.println("Result : " + max);
    }
*/}