import algo.NegAlphaBeta;
import board.IllegalPositionException;
import game.Game;
import org.junit.Test;
import util.Level;
import util.Position;

public class GameTest {

    @Test
    public void play() throws IllegalPositionException {
        // GIVEN
        Game game = new Game(new NegAlphaBeta(), Level.Normal);

        // WHEN
        game.play(new Position(3,2));
        game.play(new Position(3,5));

        // THEN
    }
}